# ACCESS_CONTROLL_BACKEND

# APP -  `ACCESS_CONTROLL_BACKEND`
==============================================


**1. Install .NetCore.**  
- https://dotnet.microsoft.com/download/dotnet-core/3.1

**2. Clone Project .**  
- git clone https://gitlab.com/enuarmunoz/accesscontrol.git
- **_clonar una rama_**
- git clone --single-branch --branch master  https://gitlab.com/enuarmunoz/accesscontrol.git
- config string connection database  **_"dataSource": "Host=localhost;Port=5432;Database=accesscontrol;User ID=postgres; Password=admin;"_**

**3. Install Dependencies.**   
- cd **_Ms.AccessControl.Core_**
- run **_donet restore_**

**3.1 Run Migrations.**   
- create bd **_accesscontrol_**
- delete all files folder **_Migrations_** from ___Ms.AccessControl.Core/Migrations___ 
- run for create migrations **_dotnet ef migrations add EmployyeMigration   --context AccessControlDbContext_**
- update database **dotnet ef database update  --context AccessControlDbContext**

**3.2 Run Project.** 
- cd **_Ms.AccessControl.Core_**
- run **_donet run_**
