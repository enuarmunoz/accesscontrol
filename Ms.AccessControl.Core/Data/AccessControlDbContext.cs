using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Ms.AccessControl.Core.Models;

namespace Ms.AccessControl.Core.Data
{
    public partial class AccessControlDbContext: Microsoft.EntityFrameworkCore.DbContext
    {

        private readonly IHttpContextAccessor httpAccessor;

        public AccessControlDbContext(IHttpContextAccessor httpAccessor, DbContextOptions<AccessControlDbContext> options):base(options)
        {
            this.httpAccessor = httpAccessor;
        }

        public AccessControlDbContext(IHttpContextAccessor httpAccessor)
        {
            this.httpAccessor = httpAccessor;
        }

        partial void OnModelBuilding(ModelBuilder builder);

        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            #region RULES
            // ================================================================

            builder.Entity<Employee>()
                  .Property(p => p.country)
                  .HasDefaultValueSql("'Colombia'");

            builder.Entity<Employee>()
                  .Property(p => p.state)
                  .HasDefaultValueSql("'Activo'");

            builder.Entity<Employee>()
                  .Property(p => p.registrationDate)
                  .HasDefaultValueSql("now()");

            #endregion

            #region SECUENCES

            // SECUENCIAS 

            builder.HasSequence("Employee_seq", schema: "public")
                .StartsAt(1)
                .IncrementsBy(1);

            builder.Entity<Employee>()
                  .Property(p => p.id)
                  .HasDefaultValueSql("nextval('\"Employee_seq\"'::regclass)");

            #endregion
            this.OnModelBuilding(builder);
        }

        public DbSet<Employee> Employees { get; set; }
    }
}