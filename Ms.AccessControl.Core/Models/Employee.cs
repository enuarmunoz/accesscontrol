﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ms.AccessControl.Core.Models
{
    [Table("Employee", Schema = "public")]
    public class Employee
    {
        [Key]
        public int id { get; set; }
        public string surname { get; set; }
        public string secondSurname { get; set; }
        public string firstName {get; set;}
        public string otherNames {get; set;}
        public string country {get; set;}
        public string idType {get; set;}
        public string identificationNumber {get; set;}
        public string email {get; set;}
        public DateTime admissionDate {get; set;}
        public string area {get; set;}
        public string state {get; set;}
        public DateTime registrationDate {get; set;}
    }
}
